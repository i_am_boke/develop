import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-org-tree',
  templateUrl: './org-tree.component.html',
  styleUrls: ['./org-tree.component.scss']
})
export class OrgTreeComponent implements OnInit {
  isOrgTreeOpen: boolean = false;
  currentClickedNode : string;
  isNodeclicked: boolean = false;
  orgTreeNode : Array<any> = [
      {
          nodeType: 'o',
          nodeName: 'Poseidon Services',
          userName: 'Harish Boke',
          nodeStatus: 0,
          nodeID: '12_45'
      },
      {
          nodeType: 'o',
          nodeName: 'Services',
          userName: 'Girish Boke',
          nodeStatus: 1,
          nodeID: '13_45'
      },
      {
          nodeType: 'o',
          nodeName: 'Domains',
          userName: 'Rupesh Raut',
          nodeStatus: 0,
          nodeID: '14_45'
      },
      {
          nodeType: 'o',
          nodeName: 'Technologies',
          userName: ' Purrushottam Korade',
          nodeStatus: 2,
          nodeID: '15_45'
      },
      {
          nodeType: 'o',
          nodeName: 'GOC\'s',
          userName: 'Kaushik V. Panchal',
          nodeStatus: 1,
          nodeID: '16_45'
      },
      {
          nodeType: 'o',
          nodeName: 'Quality Assurance',
          userName: 'Harsha Kakkeri',
          nodeStatus: 0,
          nodeID: '17_45'
      }
  ];
  SuborgTreeNode : Array<any> = [        
      {
          nodeType: 'o',
          nodeName: 'Domains',
          userName: 'Rupesh Raut',
          nodeStatus: 0,
          nodeID: '14_45'
      },
      {
          nodeType: 'o',
          nodeName: 'Technologies',
          userName: ' Purrushottam Korade',
          nodeStatus: 2,
          nodeID: '15_45'
      },
      {
          nodeType: 'o',
          nodeName: 'GOC\'s',
          userName: 'Kaushik V. Panchal',
          nodeStatus: 1,
          nodeID: '16_45'
      },
      {
          nodeType: 'o',
          nodeName: 'Quality Assurance',
          userName: 'Harsha Kakkeri',
          nodeStatus: 0,
          nodeID: '17_45'
      }
  ];
  constructor() { }

  ngOnInit() {
  }
  openOrgTree(event){
    this.isOrgTreeOpen = !this.isOrgTreeOpen;
    event.preventDefault();
    event.stopPropagation();        
}
renderOrgTree(event:any) {        
  if(event.currentTarget.id === this.currentClickedNode){
      this.isNodeclicked = !this.isNodeclicked;
  }
  else{
      this.isNodeclicked = true;
  }
  
  this.currentClickedNode = event.currentTarget.id;
  // if(event.currentTarget.classList.indexOf("active") !== -1) {
  //     this.isNodeclicked = true;
  // }
  // else {
  //     this.isNodeclicked = false;
  // }
  event.preventDefault();
  event.stopPropagation();  
 // alert("org node clicked "+ event.currentTarget.id);
}
}
