import { Component, Renderer2, OnInit, HostListener } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { SharedService } from '../../shared/services/apiServices/commonServices/shared.service';
import { OrgTreeService } from '../../shared/services/apiServices/trendServices/orgtree.service';
import { environment } from '../../../environments/environment.prod';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    isOrgTreeOpen = false;
    isNodeclicked = false;
    currentClickedNode: string;
    nodeAccessLevel: string;
    nodeAccessHierarchy: any;
    isSideBarOpen = false;
    orgTreeNode: Array<any> = [];
    sapienceVersion;
    breadCrumbData = [];
    cachedNodeDomId = '';
    cachedTreeNodes = [];
    maxZIndex = 99;
    userStatsData = {
        activeUsers : 0,
        dormantUsers : 0,
        unRegisteredUsers : 0,
        silentUsers: 0
    };

    constructor(private renderer: Renderer2, private shared: SharedService, private orgTreeService: OrgTreeService) {
        this.sapienceVersion = environment.version;
    }

    mouseEnter() {
        this.renderer.addClass(document.body, 'sidebar-menuOpen');
        this.isSideBarOpen = true;
    }

    mouseLeave() {
        this.renderer.removeClass(document.body, 'sidebar-menuOpen');
        this.isSideBarOpen = false;
    }

    openOrgTree(event) {
        this.isOrgTreeOpen = !this.isOrgTreeOpen;
        event.preventDefault();
        event.stopPropagation();
    }

    renderOrgTree(event: any) {
        if (event.currentTarget.id === this.currentClickedNode) {
            this.isNodeclicked = !this.isNodeclicked;
        } else {
            this.isNodeclicked = true;
        }

        this.currentClickedNode = event.currentTarget.id;
        // if(event.currentTarget.classList.indexOf("active") !== -1) {
        //     this.isNodeclicked = true;
        // }
        // else {
        //     this.isNodeclicked = false;
        // }
        event.preventDefault();
        event.stopPropagation();
    }

    getOrgTreeForUser() {
        let nodeIds;
        let obj, nodeObj;
        let nodeType;

        this.orgTreeNode[0] = {};
        this.orgTreeNode[0].level = 0;
        this.orgTreeNode[0].isLevelVisible = true;
        this.orgTreeNode[0].treeLevelClass = 'dropdown-nav dropdown-submenu-0';
        this.orgTreeNode[0].treeNodes = [];
        // 1- red, 2 green, 3-amber, 4-grey, 5 – line across
        obj = {
            nodeType: 'o',
            nodeName: this.shared.getUserInfo().ROI[0].NM,
            nodeTypeNumeric: 1,
            managerName: '',
            isActive: false,
            isSelected: true,
            isExpanded: false,
            isLeaf: false,
            isRoot: true,
            nodeStatus: 0,
            nodeDomID : this.shared.getUserInfo().ROI[0].NI + '__' + 1 + '__' + 0,
            nodeID: this.shared.getUserInfo().ROI[0].NI
        };
        this.orgTreeNode[0].treeNodes.push(obj);
        this.shared.breadCrumbText = obj.nodeName;

        if (this.nodeAccessHierarchy.length === 1) {
            let itemName = obj.nodeName;
            if ( obj.managerName !== '') {
                itemName += ' (' + obj.managerName + ') : ';
            }
            this.breadCrumbData.push({ breadCrumbText: itemName});
            this.updateLocalStorageWithTreeData();
            return;
        } else {
            this.breadCrumbData.push({ breadCrumbText: obj.nodeName });
            obj.isActive = true;
        }

        for (let i = 1; i < this.nodeAccessHierarchy.length; i++) {
            nodeIds = this.nodeAccessHierarchy[i].split('_');
            this.orgTreeNode[i] = {};
            this.orgTreeNode[i].level = i;
            this.orgTreeNode[i].isLevelVisible = true;
            this.orgTreeNode[i].treeLevelClass = 'dropdown-nav dropdown-submenu-' + i;
            this.orgTreeNode[i].treeNodes = [];
            let randomNodeColor = 1;
            this.orgTreeService.getChildNodesByNodeId(nodeIds[0], nodeIds[1]).subscribe(
                data => {
                    for (let x = 0; x < data.LSTNOD.length; x++) {
                        // TODO: NWWT Attribute from the payload should be used for nodeStatus (i.e. color of the node).
                        randomNodeColor = (x % 4) + 1;
                        nodeObj = data.LSTNOD[x];
                        nodeType = '';
                        if (nodeObj.NT === 1) {
                            nodeType = 'o';
                        } else if (nodeObj.NT === 2) {
                            nodeType = 'p';
                        } else if (nodeObj.NT === 3) {
                            nodeType = 't';
                        } else if (nodeObj.NT === 4) {
                            nodeType = 'u';
                        }

                        obj = {
                            nodeType: nodeType,
                            nodeTypeNumeric: nodeObj.NT,
                            nodeName: nodeObj.NM,
                            managerName: nodeObj.MGNM,
                            isRoot: false,
                            nodeStatus: randomNodeColor,
                            isActive: true,
                            isSelected: false,
                            isExpanded: false,
                            isLeaf: false,
                            nodeDomID : nodeObj.NI + '__' + nodeObj.NT + '__' + i,
                            nodeID: nodeObj.NI
                        };
                        if (nodeObj.NT === 4) {
                            obj.isLeaf = true;
                        }
                        if ( i + 1 === this.nodeAccessHierarchy.length) {
                            obj.isActive = false;
                            obj.isSelected = true;
                        }
                        this.orgTreeNode[i].treeNodes.push(obj);
                    }
                    this.updateLocalStorageWithTreeData();
                },
                error => {
                    console.log(error);
                }
            );
        }
    }

    cacheNextLevelTreeNodes(currLevel, nodeDomID) {
        const nextLevel = currLevel + 1;
        this.cachedNodeDomId = nodeDomID;
        this.cachedTreeNodes = this.orgTreeNode[nextLevel].treeNodes;
    }

    updateLocalStorageWithTreeData() {
        localStorage.setItem('orgTreeNode', JSON.stringify(this.orgTreeNode));
        localStorage.setItem('selectedNodeId', this.shared.selectedNodeId);
        localStorage.setItem('selectedNodeType', this.shared.selectedNodeType);
        localStorage.setItem('breadCrumbData', JSON.stringify(this.breadCrumbData));
    }

    setTreeNodeAttributes(nodeId, nodeType, currLevel, nodeDomID, isExpanded) {
        const nextLevel = currLevel + 1;
        this.setExpandedClassForNode(nodeDomID);
        this.currentClickedNode = nodeId;
        this.orgTreeNode.splice(currLevel + 1, this.orgTreeNode.length);
        this.orgTreeNode[nextLevel] = {};
        this.orgTreeNode[nextLevel].level = nextLevel;
        this.orgTreeNode[nextLevel].isLevelVisible = true;
        this.orgTreeNode[nextLevel].treeLevelClass = 'dropdown-nav dropdown-submenu-' + nextLevel;
        this.orgTreeNode[nextLevel].treeNodes = [];
    }

    loadChildTreeFromCache(nextLevel, nodeDomID) {

        if (this.cachedTreeNodes.length === 0) {
            this.setLeafClassForNode(nodeDomID);
            return;
        }
        for (let x = 0; x < this.cachedTreeNodes.length; x++) {
            this.orgTreeNode[nextLevel].treeNodes.push(this.cachedTreeNodes[x]);
        }
    }

    loadChildTree(nodeId, nodeType, currLevel, nodeDomID, isExpanded) {
        const nextLevel = currLevel + 1;
        if (isExpanded) {
            this.cacheNextLevelTreeNodes(currLevel, nodeDomID);
            this.removeExpandedClassForNode(nodeDomID);
            // hide next level's tree
            // this.orgTreeNode[nextLevel].isLevelVisible = false;
            return;
        }

        if (nodeDomID === this.cachedNodeDomId) {
            this.setTreeNodeAttributes(nodeId, nodeType, currLevel, nodeDomID, isExpanded);
            this.loadChildTreeFromCache(nextLevel, nodeDomID);
            return;
        }

        this.setTreeNodeAttributes(nodeId, nodeType, currLevel, nodeDomID, isExpanded);
        // this.orgTreeNode[currLevel].selectedNodeId = nodeId;

        let nodeObj, obj;
        this.orgTreeService.getChildNodesByNodeId(nodeId, nodeType).subscribe(
            data => {
                if (data.LSTNOD.length === 0) {
                    this.setLeafClassForNode(nodeDomID);
                    this.updateLocalStorageWithTreeData();
                    return;
                }

                let randomNodeColor = 1;
                // TODO: NWWT Attribute from the payload should be used for nodeStatus (i.e. color of the node).
                for (let x = 0; x < data.LSTNOD.length; x++) {
                    randomNodeColor = (x % 4) + 1;
                    nodeObj = data.LSTNOD[x];
                    nodeType = '';
                    if (nodeObj.NT === 1) {
                        nodeType = 'o';
                    } else if (nodeObj.NT === 2) {
                        nodeType = 'p';
                    } else if (nodeObj.NT === 3) {
                        nodeType = 't';
                    } else if (nodeObj.NT === 4) {
                        nodeType = 'u';
                    }

                    obj = {
                        nodeType: nodeType,
                        nodeTypeNumeric: nodeObj.NT,
                        nodeName: nodeObj.NM,
                        managerName: nodeObj.MGNM,
                        nodeStatus: randomNodeColor,
                        nodeID: nodeObj.NI,
                        isActive: false,
                        isRoot: false,
                        isSelected: false,
                        isExpanded: false,
                        nodeDomID : nodeObj.NI + '__' + nodeObj.NT + '__' + nextLevel
                    };
                    obj.isLeaf = false;
                    if ( nodeObj.NT === 4) {
                        obj.isLeaf = true;
                    }

                    this.orgTreeNode[nextLevel].treeNodes.push(obj);
                }
                this.updateLocalStorageWithTreeData();
            },
            error => {
                console.log(error);
            }
        );
    }

    setExpandedClassForNode(nodeDomId) {
        const nodeInfo = nodeDomId.split('__');
        const treeLevel = nodeInfo[2];
        let treeNodesOfOrgTree = this.orgTreeNode[treeLevel].treeNodes;
        for (let i = 0; i < treeNodesOfOrgTree.length; i++) {
            treeNodesOfOrgTree[i].isExpanded = false;
            if (treeNodesOfOrgTree[i].nodeDomID === nodeDomId) {
                treeNodesOfOrgTree[i].isExpanded = true;
            }
        }
    }

    removeExpandedClassForNode(nodeDomId) {
        const nodeInfo = nodeDomId.split('__');
        const treeLevel = parseInt(nodeInfo[2], 10);

        for (let j = treeLevel; j < this.orgTreeNode.length; j++ ) {
            if (j + 1 < this.orgTreeNode.length) {
                this.orgTreeNode[j + 1].isLevelVisible = false;
            }

            let treeNodesOfOrgTree = this.orgTreeNode[j].treeNodes;
            for (let i = 0; i < treeNodesOfOrgTree.length; i++) {
                treeNodesOfOrgTree[i].isExpanded = false;
            }
        }
    }

    setLeafClassForNode(nodeDomId) {
        const nodeInfo = nodeDomId.split('__');
        const treeLevel = nodeInfo[2];
        let treeNodesOfOrgTree = this.orgTreeNode[treeLevel].treeNodes;
        for (let i = 0; i < treeNodesOfOrgTree.length; i++) {
            if (treeNodesOfOrgTree[i].nodeDomID === nodeDomId) {
                treeNodesOfOrgTree[i].isLeaf = true;
            }
        }
    }

    setSelectedClassForIntermediateNodes(level) {
        const countOfItems = this.breadCrumbData.length;
        for (let l = countOfItems; l < level; l++) {
            const treeNodesOfOrgTree = this.orgTreeNode[l].treeNodes;
            for (let i = 0; i < treeNodesOfOrgTree.length; i++) {
                treeNodesOfOrgTree[i].isSelected = false;
                if (treeNodesOfOrgTree[i].isExpanded) {
                    treeNodesOfOrgTree[i].isSelected = true;
                    break;
                }
            }
        }
    }

    setSelectedClassForNode(nodeDomId, node, level) {
        if (level > this.breadCrumbData.length) {
            this.setSelectedClassForIntermediateNodes(level);
        }
        const nodeInfo = nodeDomId.split('__');
        const treeLevel = parseInt(nodeInfo[2], 10);
        let treeNodesOfOrgTree = this.orgTreeNode[treeLevel].treeNodes;
        for (let i = 0; i < treeNodesOfOrgTree.length; i++) {
            treeNodesOfOrgTree[i].isSelected = false;
            if (treeNodesOfOrgTree[i].nodeDomID === nodeDomId) {
                treeNodesOfOrgTree[i].isSelected = true;
            }
        }
    }

    removeSelectedClassForNode(nodeDomId) {
        const nodeInfo = nodeDomId.split('__');
        const treeLevel = parseInt(nodeInfo[2], 10);
        for (let j = treeLevel; j < this.orgTreeNode.length; j++) {
            let treeNodesOfOrgTree = this.orgTreeNode[j].treeNodes;
            for (let i = 0; i < treeNodesOfOrgTree.length; i++) {
                treeNodesOfOrgTree[i].isSelected = false;
            }
        }
    }

    selectTreeNode(node, level) {
        this.removeSelectedClassForNode(node.nodeDomID);
        this.setSelectedClassForNode(node.nodeDomID, node, level);
        this.updateBreadCrumbData(node, level);
        this.updateSelectedNode(node.nodeID, node.nodeTypeNumeric);
        this.updateUserStatistics(node.nodeID, node.nodeTypeNumeric);
        this.updateLocalStorageWithTreeData();
    }

    updateUserStatistics(nodeId: number, nodeType: number) {
        this.orgTreeService.getUserStatistics(nodeId, nodeType).subscribe(
            data => {
                for ( let i = 0; i < data.LSTUC.length; i++) {
                    if (data.LSTUC[i].UT === 'Active Users') {
                        this.userStatsData.activeUsers = data.LSTUC[i].CNT;
                    } else if (data.LSTUC[i].UT === 'Dormant Users') {
                        this.userStatsData.dormantUsers = data.LSTUC[i].CNT;
                    } else if (data.LSTUC[i].UT === 'Non-Registered Users') {
                        this.userStatsData.unRegisteredUsers = data.LSTUC[i].CNT;
                    } else if (data.LSTUC[i].UT === 'On Vacation') {
                        this.userStatsData.silentUsers = data.LSTUC[i].CNT;
                    }
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    removeManagerNameFromLastBreadCrumb() {
        let item = this.breadCrumbData[this.breadCrumbData.length - 1].breadCrumbText;
        let index = item.indexOf('(');
        if (index > -1) {
            item = item.substring(0, index);
            this.breadCrumbData[this.breadCrumbData.length - 1].breadCrumbText = item.trim();
        }
    }

    updateBreadCrumbData(node, level) {
        // Child is clicked -- if 2 items in breadCrumb and level 2 is clicked, means node of actually 3rd box is clicked.
        // Remove the name of manager from the bread crumb text.
        if (level === this.breadCrumbData.length) {
            this.removeManagerNameFromLastBreadCrumb();
        }

        if (level > this.breadCrumbData.length) {
            this.removeManagerNameFromLastBreadCrumb();
            const countOfItems = this.breadCrumbData.length;
            for (let l = countOfItems; l < level; l++) {
                const treeNodesOfOrgTree = this.orgTreeNode[l].treeNodes;
                for (let i = 0; i < treeNodesOfOrgTree.length; i++) {
                    if (treeNodesOfOrgTree[i].isExpanded) {
                        this.breadCrumbData.push({breadCrumbText : treeNodesOfOrgTree[i].nodeName});
                        break;
                    }
                }
            }
        }

        if (level < this.breadCrumbData.length) {
            this.breadCrumbData.splice(level, this.breadCrumbData.length);
        }

        var itemName = node.nodeName;
        if (node.managerName !== '') {
            itemName += ' (' + node.managerName + ') : ';
        }
        this.breadCrumbData.push({breadCrumbText: itemName});
        this.shared.breadCrumbText = node.nodeName;
    }

    updateSelectedNode(nodeId, nodeType) {
        this.shared.selectedNodeId = nodeId;
        this.shared.selectedNodeType = nodeType;
    }

    updateBreadCrumbForSharedServiceFromLS() {
        let item = this.breadCrumbData[this.breadCrumbData.length - 1].breadCrumbText;
        let index = item.indexOf('(');
        if (index > -1) {
            this.shared.breadCrumbText = item.substring(0, index);
        }
    }

    updateSelectedNodeFromLS() {
        this.shared.selectedNodeId = localStorage.getItem('selectedNodeId');
        this.shared.selectedNodeType = localStorage.getItem('nodeType');
    }

    updateBreadCrumbFromLS() {
        this.breadCrumbData = JSON.parse(localStorage.getItem('breadCrumbData'));
        this.updateBreadCrumbForSharedServiceFromLS();
    }

    updateOrgTreeFromLS() {
        this.orgTreeNode = JSON.parse(localStorage.getItem('orgTreeNode'));
    }

    getNodeAccessLevelForUser() {
        this.orgTreeService.getHigestLevelNodeAccess().subscribe(
            data => {
                    this.nodeAccessLevel = data.LSTNO[0].HRRC;
                    this.nodeAccessHierarchy = this.nodeAccessLevel.split('\\');
                    this.nodeAccessHierarchy.splice(0, 1);
                    this.nodeAccessHierarchy.splice(this.nodeAccessHierarchy.length - 1, 1);
                    const nodeData = this.nodeAccessHierarchy[this.nodeAccessHierarchy.length - 1];
                    const defaultSelectedNode = nodeData.split('_');
                    const nodeDomId = defaultSelectedNode[0] + '__' + defaultSelectedNode[1] + '__' + 0;
                    this.updateSelectedNode(parseInt(defaultSelectedNode[0], 10), parseInt(defaultSelectedNode[1], 10));
                    this.updateUserStatistics(parseInt(defaultSelectedNode[0], 10), parseInt(defaultSelectedNode[1], 10));
                    this.getOrgTreeForUser();
                },
            error => {
                    console.log(error);
                }
        );
    }

    ngOnInit() {
        if (localStorage.getItem('orgTreeNode')) {
            this.updateSelectedNodeFromLS();
            this.updateBreadCrumbFromLS();
            this.updateOrgTreeFromLS();
            this.updateUserStatistics(this.shared.selectedNodeId, this.shared.selectedNodeType);
        } else {
            this.getNodeAccessLevelForUser();
        }
    }

    clickedInsideTree($event: Event) {
        // $event.preventDefault();
        $event.stopPropagation();  // <- that will stop propagation on lower layers
    }
    @HostListener('document:click', ['$event']) clickedOutsideTree($event) {
        //this.resetDrawerSelection();
        if (this.isOrgTreeOpen) {
          this.isOrgTreeOpen = false;
        }
      }

}
