import { Component, Inject, OnInit, Renderer2, HostListener } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { APP_CONFIG, AppConfig } from '../../config/app.config';
import { IAppConfig } from '../../config/iapp.config';
import { ProgressBarService } from '../progress-bar.service';
import { SharedService } from '../../shared/services/apiServices/commonServices/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})

export class NavComponent implements OnInit {
  appConfig: any;
  menuItems: any[];
  progressBarMode: string;
  isDrawerOpen = false;
  userProfileContent = false;
  userSaveContent = false;
  userReportContent = false;
  userConfigureContent = false;
  userProfile: any;
  userName = '';
  imgBase64 = '';
  lastLoginDateTime = '';

  private translateService: TranslateService;

  constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
              private progressBarService: ProgressBarService,
              translateService: TranslateService, private renderer: Renderer2,
              private sharedService: SharedService, private router: Router) {
    this.appConfig = appConfig;
    this.translateService = translateService;
    this.loadMenus();
    this.progressBarService.updateProgressBar$.subscribe((mode: string) => {
      this.progressBarMode = mode;
    });
  }

  ngOnInit() {

  }

  changeLanguage(language: string): void {
    this.translateService.use(language).subscribe(() => {
      this.loadMenus();
    });
  }

  resetDrawerSelection() {
    this.userProfileContent = false;
    this.userSaveContent = false;
    this.userReportContent = false;
    this.userConfigureContent = false;
  }

  openSidebarWrapper(event, contentName) {
    this.isDrawerOpen = true;
    event.preventDefault();
    event.stopPropagation();

    this.resetDrawerSelection();

    if (this.isDrawerOpen === true) {
      switch (contentName) {
        case 'userProfile':
            this.userProfile = this.sharedService.getUserInfo();
            this.userName = this.userProfile.USI.NM;
            this.imgBase64 = this.userProfile.USI.IMG;
            this.lastLoginDateTime = this.userProfile.USI.LSL;
            this.userProfileContent = true;
        break;
        case 'userSave':
          this.userSaveContent = true;
        break;
        case 'userReport':
          this.userReportContent = true;
        break;
        case 'userConfigure':
          this.userConfigureContent = true;
        break;
        default:
          break;
      }
    }
  }

  logout() {
      localStorage.removeItem('currentUser');
      localStorage.removeItem('orgTreeNode');
      localStorage.removeItem('selectedNodeId');
      localStorage.removeItem('selectedNodeType');
      localStorage.removeItem('breadCrumbData');
      localStorage.removeItem('selecteDateRange');
      this.router.navigate(['/login']);
  }

  clickedInside($event: Event) {
    // $event.preventDefault();
    $event.stopPropagation();  // <- that will stop propagation on lower layers
  }

  @HostListener('document:click', ['$event']) clickedOutside($event) {
    this.resetDrawerSelection();
    if (this.isDrawerOpen) {
      this.isDrawerOpen = false;
    }
  }

  private loadMenus(): void {
    this.translateService.get(['dashboard', 'dashboard'], {}).subscribe((texts: any) => {
      this.menuItems = [
        {link: '/', name: texts['dashboard']},
        {link: '/' + AppConfig.routes.dashboard, name: texts['dashboard']}
      ];
    });
  }
}
