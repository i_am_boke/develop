import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { APP_CONFIG, AppConfig } from './config/app.config';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/modules/shared.module';
import { CoreModule } from './core/core.module';
import { AppComponent } from './app.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from './app.translate.factory';
import { ProgressBarService } from './core/progress-bar.service';
import { ProgressInterceptor } from './shared/interceptors/progress.interceptor';
import { TimingInterceptor } from './shared/interceptors/timing.interceptor';
import { SidebarComponent } from './core/sidebar/sidebar.component';
import { LoginCarouselComponent } from './core/login-carousel/login-carousel.component';
import { LoginComponent } from './UI/common/login/login.component';
import { ForgotPasswordComponent } from './UI/common/forgot-password/forgot-password.component';
import { ConfirmPasswordComponent } from './UI/common/confirm-password/confirm-password.component';
import { MainComponent } from './main/main.component';
import { AuthService } from './shared/services/apiServices/commonServices/auth.service';
import { SharedService } from './shared/services/apiServices/commonServices/shared.service';
import { UtilService } from './shared/services/apiServices/commonServices/util.service';
import { OrgTreeService } from './shared/services/apiServices/trendServices/orgtree.service';
import { DashboardService } from './shared/services/apiServices/trendServices/dashboard.service';
import { DashboardModule} from './UI/trendUI/dashboard/dashboard.module';
import { TrendsComponent } from './UI/trendUI/trends/trends.component';
import { PowerAnalyticsComponent } from './UI/trendUI/power-analytics/power-analytics.component';
import { ChartModule } from 'angular-highcharts';
import { NgxCarouselModule } from 'ngx-carousel';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgxCarouselModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    SharedModule.forRoot(),
    CoreModule,
    AppRoutingModule,
    DashboardModule, ChartModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  declarations: [
    AppComponent,
    SidebarComponent,
    LoginComponent, ForgotPasswordComponent, ConfirmPasswordComponent,
    LoginCarouselComponent,
    MainComponent,
    TrendsComponent,
    PowerAnalyticsComponent
    ],
  providers: [
      { provide: APP_CONFIG, useValue: AppConfig },
      { provide: LocationStrategy, useClass: HashLocationStrategy },
      { provide: HTTP_INTERCEPTORS, useClass: ProgressInterceptor, multi: true, deps: [ProgressBarService] },
      { provide: HTTP_INTERCEPTORS, useClass: TimingInterceptor, multi: true },
      AuthService, SharedService, DashboardService, OrgTreeService, UtilService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
