import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { UtilService } from './util.service';
import { AuthResponse } from '../../../entities/auth.model';

@Injectable()
export class AuthService {
    constructor(private http: HttpClient, private utils: UtilService) { }

    login(userName: string, pwd: string): Observable<AuthResponse> {
        const url = this.utils.getBaseUrl() + '/Trends/ReqAuthenticateUser';
        const authObj = {UN : userName, WD: pwd};
        let header: any = new HttpHeaders();
        header.append('Content-Type', 'application/json; charset=utf-8');
        const httpOptions = { headers: header, url: url};
        return this.http.post<AuthResponse>(url, authObj, httpOptions);
    }
}
