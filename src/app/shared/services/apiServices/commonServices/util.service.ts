import { Injectable } from '@angular/core';
import { AppConfig } from '../../../../config/app.config';

@Injectable()
export class UtilService {
    baseUrl: string;
    getISODateString(dt) {
        const dateFormatted = dt.substr(0, dt.indexOf('T'));
        return dateFormatted;
    }

    getBaseUrl() {
        this.baseUrl = AppConfig.endpoints.protocol + '://' +
            AppConfig.endpoints.remoteDomain + ':443' + '/' + AppConfig.endpoints.baseVirtualDir;
        return this.baseUrl;
    }
}
