import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {
    selectedDateRange;
    selectedNodeId;
    selectedNodeType;
    selectedDateRangeText;
    breadCrumbText;

    setSelectedDateRangeOnLogin() {
        var d = new Date();
        d.setDate(d.getDate() - 7);
        d.setUTCHours(0, 0, 0, 0);
        this.selectedDateRange = {
            'ID': 1,
            'TX': '1 WK',
            'SD': d.toISOString(),
            'ED': new Date().toISOString()
        };
    }

    setSelectedDateRange(dt) {
        this.selectedDateRange = dt;
    }

    getSelectedDateRange() {
        return this.selectedDateRange;
    }

    getSelectedDateRangeText() {
        const objStartDate = new Date(this.selectedDateRange.SD);
        const objEndDate = new Date(this.selectedDateRange.ED);
        const locale = 'en-us';
        let startMonth = objStartDate.toLocaleString(locale, {month: 'long'}) + ' ';
        let startDate = objStartDate.toLocaleString(locale, {day: 'numeric'}) + ' - ';
        let endDate = objEndDate.toLocaleString(locale, {day: 'numeric'}) + ', ';
        let month = objEndDate.toLocaleString(locale, {month: 'long'}) + ' ';
        let year = objEndDate.toLocaleString(locale, {year: 'numeric'});
        this.selectedDateRangeText = (startMonth + startDate + month + endDate + year);
        return this.selectedDateRangeText;
    }

    getUserInfo() {
        return JSON.parse(localStorage.getItem('currentUser'));
    }


}
