import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SharedService } from '../commonServices/shared.service';
import { UtilService } from '../commonServices/util.service';
import { UserStatistics } from '../../../entities/userstats.model';

@Injectable()
export class OrgTreeService {
    constructor(private http: HttpClient, private sharedService: SharedService, private utils: UtilService) { }

    getHigestLevelNodeAccess() {
        const url = this.utils.getBaseUrl() + '/Trends/ReqUsersHighestNode';
        let header: any = new HttpHeaders();
        const inputObj = { 'ST' : this.sharedService.getUserInfo().ST, 'XT' : '' };
        header.append('Content-Type', 'application/json; charset=utf-8');
        const httpOptions = { headers: header, url: url};
        return this.http.post<any>(url, inputObj, httpOptions)
            .map(data => {
                return data;
            });
    }

    getUserStatistics(nodeId, nodeType) {
        const url = this.utils.getBaseUrl() + '/Trends/ReqUserCountStatistics';
        let header: any = new HttpHeaders();
        let obj = {
            'ST' : this.sharedService.getUserInfo().ST,
            'XT' : '',
            'NI' : nodeId,
            'NT' : nodeType,
            'SD' : this.utils.getISODateString(this.sharedService.getSelectedDateRange().ED),
            'ED' : this.utils.getISODateString(this.sharedService.getSelectedDateRange().ED),
            'TTU' : 1,
            'RGU' : 1,
            'DRU' : 1,
            'ACU' : 1,
            'AWU' : 1,
            'OVU' : 1,
            'DMU' : 1,
            'DPU' : 1,
            'WCU' : 1,
            'NRU' : 1,
            'SH' : 1
        };
        header.append('Content-Type', 'application/json; charset=utf-8');
        const httpOptions = { headers: header, url: url};
        return this.http.post<UserStatistics>(url, obj, httpOptions)
            .map(data => {
                return data;
            });
    }

    getChildNodesByNodeId(nodeId, nodeType) {
        const url = this.utils.getBaseUrl() + '/Trends/ReqOrgTreeForTrends';
        let header: any = new HttpHeaders();
        header.append('Content-Type', 'application/json; charset=utf-8');
        let obj = {
            'ST': this.sharedService.getUserInfo().ST,
            'XT': '',
            'SD': this.utils.getISODateString(this.sharedService.getSelectedDateRange().SD),
            'ED': this.utils.getISODateString(this.sharedService.getSelectedDateRange().ED),
            'NI': nodeId,
            'NT': nodeType
        };
        const httpOptions = { headers: header, url: url};
        return this.http.post<any>(url, obj, httpOptions)
            .map(data => {
                return data;
            });
    }
}
