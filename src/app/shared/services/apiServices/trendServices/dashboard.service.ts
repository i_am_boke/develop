import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SharedService } from '../commonServices/shared.service';
import { UtilService } from '../commonServices/util.service';

@Injectable()
export class DashboardService {
    constructor(private http: HttpClient, private sharedService: SharedService, private utils: UtilService) { }

    getLineChartByNodeIdByDate(nodeId, nodeType) {
        const url = this.utils.getBaseUrl() + '/Trends/ReqDashActivity';
        let header: any = new HttpHeaders();
        header.append('Content-Type', 'application/json; charset=utf-8');
        // Here TT is 0 means, its line chart.
        // TODO: The backend doesnt have data for current dates, hence currently hard coded the dates.
        // TODO: UI means userID of the logged-in user, this is missing in the login api.
        let obj = {
            'ST': this.sharedService.getUserInfo().ST,
            'XT': '',
            'NI': nodeId,
            'NT': nodeType,
            'SD': this.utils.getISODateString(this.sharedService.getSelectedDateRange().SD),
            'ED': this.utils.getISODateString(this.sharedService.getSelectedDateRange().ED),
            'DT': 1,
            'TT': 0,
            'UI': 2492,
            'CI': '',
            'F1': '',
            'F2': '',
            'PS': 0,
            'PI': 0,
            'XM': true,
            'XV': 0,
            'Xo': true,
            'XP': true,
            'XC': true,
            'OO': false,
            'SH': true,
            'XU': true,
            'SO': 0
        };

        const httpOptions = { headers: header, url: url};
        return this.http.post<any>(url, obj, httpOptions)
            .map(data => {
                return data;
            });
    }

    getDonutChartAndKPI(nodeId, nodeType) {
        const url = this.utils.getBaseUrl() + '/Trends/ReqWorkSummary';
        let header: any = new HttpHeaders();
        header.append('Content-Type', 'application/json; charset=utf-8');
        let obj = {
            'ST': this.sharedService.getUserInfo().ST,
            'XT': '',
            'NI': nodeId,
            'NT': nodeType,
            'SD': this.utils.getISODateString(this.sharedService.getSelectedDateRange().SD),
            'ED': this.utils.getISODateString(this.sharedService.getSelectedDateRange().ED),
            'DT': 1,
            'TT': 0,
            'UI': 2492,
            'CI': '',
            'F1': '1,2,3,4,5,6',
            'F2': '',
            'PS': 0,
            'PI': 0,
            'XM': true,
            'XV': 0,
            'Xo': true,
            'XP': true,
            'XC': true,
            'OO': false,
            'SH': true,
            'XU': true,
            'SO': 0
        };
        const httpOptions = { headers: header, url: url};
        return this.http.post<any>(url, obj, httpOptions)
            .map(data => {
                return data;
            });
    }

    getHeatMapChart(nodeId, nodeType) {
        // For Heat map chart, DT is 3. AND F1 should be empty string.
        // TT is 1 for Activity and 2 for Application.
        // TODO: Hardcoded SD and ED passed as there is no data in the backend for other dates.
        const url = this.utils.getBaseUrl() + '/Trends/ReqDashActivity';
        let header: any = new HttpHeaders();
        header.append('Content-Type', 'application/json; charset=utf-8');
        var obj = {
            'ST': this.sharedService.getUserInfo().ST,
            'XT': '',
            'NI': nodeId,
            'NT': nodeType,
            'SD': this.utils.getISODateString(this.sharedService.getSelectedDateRange().SD),
            'ED': this.utils.getISODateString(this.sharedService.getSelectedDateRange().ED),
            'DT': 3,
            'TT': 1,
            'UI': 2492,
            'CI': '',
            'F1': '',
            'F2': '',
            'PS': 0,
            'PI': 0,
            'XM': true,
            'XV': 0,
            'Xo': true,
            'XP': true,
            'XC': true,
            'OO': false,
            'SH': true,
            'XU': true,
            'SO': 0
        };
        const httpOptions = { headers: header, url: url};
        return this.http.post<any>(url, obj, httpOptions)
            .map(data => {
                return data;
            });
    }

    getUserCountStats() {
        const url = this.utils.getBaseUrl() + '/Trends/ReqDashboardDates';
        let header: any = new HttpHeaders();
        header.append('Content-Type', 'application/json; charset=utf-8');
        let obj = {
            'ST': this.sharedService.getUserInfo().ST,
            'XT': '',
            'NI': 1,
            'NT': 1,
            'SD': this.utils.getISODateString(this.sharedService.getSelectedDateRange().SD),
            'ED': this.utils.getISODateString(this.sharedService.getSelectedDateRange().ED),
        };
        const httpOptions = { headers: header, url: url};
        return this.http.post<any>(url, obj, httpOptions)
            .map(data => {
                return data;
            });
    }

    getDateRanges() {
        const url = this.utils.getBaseUrl() + '/Trends/ReqDashboardDates';
        let header: any = new HttpHeaders();
        header.append('Content-Type', 'application/json; charset=utf-8');
        let obj = {
            'ST': this.sharedService.getUserInfo().ST,
            'XT' : ''
        };
        const httpOptions = { headers: header, url: url};
        return this.http.post<any>(url, obj, httpOptions)
            .map(data => {
                return data;
            });
    }
}
