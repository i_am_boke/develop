export class UserStatistics {
    LSTUC: Statistics[];
    RSV: number;
    RSM?: any;
    HRS: boolean;
    HRC: number;
    HRM: string;
    SBV: string;
    ST: string;
    XT: string;
}

export class Statistics {
    DA: string;
    CNT: number;
    UT: string;
}
