export class AuthResponse {
    ROI: TokenInfo[];
    USI: {
        NM: string;
        MID: string;
        IMG: string;
        LSL: string;
    };
    RSV: number;
    RSM?: any;
    HRS: boolean;
    HRC: number;
    HRM: string;
    SBV: string;
    ST: string;
    XT: string;
}

export class TokenInfo {
    NM: string;
    NI: string;
    ST: string;
    NT: number;
}

