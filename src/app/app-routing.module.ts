import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { LoginComponent } from './UI/common/login/login.component';
import { AppConfig } from './config/app.config';
import { Error404Component } from './core/error404/error-404.component';
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './UI/trendUI/dashboard/dashboard.component';
import { PowerAnalyticsComponent } from './UI/trendUI/power-analytics/power-analytics.component';
import { TrendsComponent } from './UI/trendUI/trends/trends.component';
import {ForgotPasswordComponent} from './UI/common/forgot-password/forgot-password.component';
import {ConfirmPasswordComponent} from './UI/common/confirm-password/confirm-password.component';

const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full'},
    { path: AppConfig.routes.login, component: LoginComponent},
    { path: AppConfig.routes.loginReset, component: LoginComponent},
    { path: AppConfig.routes.forgotPassword, component: ForgotPasswordComponent },
    { path: AppConfig.routes.confirmPassword, component: ConfirmPasswordComponent },
    { path: AppConfig.routes.main, component: MainComponent,
      children: [
          {path: AppConfig.routes.dashboard, component: DashboardComponent},
          {path: AppConfig.routes.powerAnalytics, component: PowerAnalyticsComponent},
          {path: AppConfig.routes.trends, component: TrendsComponent}
      ]},

    { path: AppConfig.routes.error404, component: Error404Component},

  // otherwise redirect to 404
  {path: '**', redirectTo: '/' + AppConfig.routes.error404}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}
