import {InjectionToken} from '@angular/core';

import {IAppConfig} from './iapp.config';

export let APP_CONFIG = new InjectionToken('app.config');

export const AppConfig: IAppConfig = {
  routes: {
      dashboard: 'dashboard',
      error404: '404',
      login: 'login',
      loginReset: 'login/:reset',
      forgotPassword: 'forgot-password',
      confirmPassword: 'confirm-password',
      main: 'main',
      trends: 'trends',
      powerAnalytics: 'power-analytics'
  },
  endpoints: {
      protocol: 'https',
      localDomain: 'localhost',
      remoteDomain : 'galaxy.sapience.net',
      baseVirtualDir: 'SapienceAPIs'
  },
  votesLimit: 3,
  snackBarDuration: 3000,
  repositoryURL: 'https://bitbucket.org/i_am_boke/develop/'
};
