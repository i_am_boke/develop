import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerAnalyticsComponent } from './power-analytics.component';

describe('PowerAnalyticsComponent', () => {
  let component: PowerAnalyticsComponent;
  let fixture: ComponentFixture<PowerAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PowerAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PowerAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
