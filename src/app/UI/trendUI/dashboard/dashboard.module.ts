import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ChartModule } from 'angular-highcharts';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {SharedModule} from '../../../shared/modules/shared.module';

import {DashboardService} from '../../../shared/services/apiServices/trendServices/dashboard.service';
import {DashboardComponent} from './dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
      ChartModule,
    DashboardRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    DashboardComponent
  ],
  entryComponents: [
  ],
  providers: [
    DashboardService
  ]
})

export class DashboardModule {
}
