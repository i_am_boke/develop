import { Component, OnInit, ElementRef, ViewChild, AfterViewInit  } from '@angular/core';
import { chart } from 'highcharts';
import * as Highcharts from 'highcharts';
declare var require: any;
require('highcharts/highcharts-more')(Highcharts);
require('highcharts/modules/solid-gauge')(Highcharts);
require('highcharts/modules/heatmap')(Highcharts);
require('highcharts/modules/treemap')(Highcharts);
require('highcharts/modules/funnel')(Highcharts);
import { DashboardService } from '../../../shared/services/apiServices/trendServices/dashboard.service';
import { SharedService } from '../../../shared/services/apiServices/commonServices/shared.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})

export class DashboardComponent implements OnInit, AfterViewInit {
    @ViewChild('heatMapChartTarget', {read: ElementRef}) heatMapChartTarget: ElementRef;
    @ViewChild('lineChartTarget') lineChartTarget: ElementRef;
    @ViewChild('donughtChartTarget') donughtChartTarget: ElementRef;
    chart: Highcharts.ChartObject;
    currentClickedChart = 'highChart1';
    treeMapData;
    dateRanges = [];
    selecteDateRange;
    selecteDateRangeText = '';
    lineChartRecords = [];
    donutChartData;
    kpiItems = {
        workTime: '',
        workTimePercent: '',
        workTimeLoss: '',
        workTimeLossPercent: '',
        attendance: '',
        attendancePercent: ''
    };

    constructor(private dashboardService: DashboardService, private sharedService: SharedService) {
    }

    ngOnInit() {
        // this.getUserCountStatistics();
        this.getDashboardDateRanges();
    }

    ngAfterViewInit() {
        this.loadHeatmapChart();
        this.loadDonutChart();
    }

    getUserCountStatistics() {
        this.dashboardService.getUserCountStats().subscribe(
            data => {
            },
            error => {
                console.log(error);
            }
        );
    }

    getDashboardDateRanges() {
        this.dashboardService.getDateRanges().subscribe(
            data => {
                this.dateRanges = data.LSTDT;
                this.selecteDateRange = this.dateRanges[0];
                localStorage.setItem('selecteDateRange', this.selecteDateRange);
                this.sharedService.setSelectedDateRange(this.selecteDateRange);
                this.selecteDateRangeText = this.sharedService.getSelectedDateRangeText();
            },
            error => {
                console.log(error);
            }
        );
    }

    dateRangeClicked(dt) {
        event.preventDefault();
        event.stopPropagation();
        this.selecteDateRange = dt;
        localStorage.setItem('selecteDateRange', this.selecteDateRange);
        this.sharedService.setSelectedDateRange(dt);
        this.selecteDateRangeText = this.sharedService.getSelectedDateRangeText();
        if (this.currentClickedChart  === 'highChart2') {
            this.loadLineChart();
        }
        if (this.currentClickedChart  === 'highChart1') {
            this.loadHeatmapChart();
        }

        this.loadDonutChart();
    }

    changeView(event, chartName) {
        event.preventDefault();
        event.stopPropagation();
        this.currentClickedChart = chartName;
        if (chartName === 'highChart2') {
            this.loadLineChart();
        }
        if (chartName === 'highChart1') {
            this.loadHeatmapChart();
        }
    }

    loadHeatmapChart() {
        this.dashboardService.getHeatMapChart(this.sharedService.selectedNodeId, this.sharedService.selectedNodeType).subscribe(
            data => {
                let records = data.LSTTD;
                this.treeMapData = [];
                let obj;
                for (let i = 0; i < records.length; i++) {
                    obj = {};
                    obj.name = records[i].NM;
                    obj.color = '#41b3ff';
                    if (records[i].ET === 0) {
                        obj.color = '#4BDDF5';
                    }
                    obj.value = parseInt(records[i].TS.toFixed(2));
                    this.treeMapData.push(obj);
                }

                this.chart = chart(this.heatMapChartTarget.nativeElement, {
                    series: [{
                        type: 'treemap',
                        data: this.treeMapData
                    }],
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    }
                });
            },
            error => {
                console.log(error);
            }
        );
    }

    loadLineChart() {
        this.dashboardService.getLineChartByNodeIdByDate(this.sharedService.selectedNodeId, this.sharedService.selectedNodeType).subscribe(
            data => {
                const lineColors = ['#7e57c2', '#869aff', '#b34892', '#dcb2ff'];
                const records = data.LSTTD;
                let obj, val, colorIndex, seriesIndex;
                for (let i = 0; i < records.length; i++) {
                    colorIndex = i % 4;
                    val = parseInt(records[i].TS);
                    seriesIndex = this.getItemIndexbyName(records[i].NM);
                    if (seriesIndex === -1) {
                        obj = {
                            name : records[i].NM,
                            color: lineColors[colorIndex],
                            data : [val]
                        };
                        this.lineChartRecords.push(obj);
                    } else {
                        this.lineChartRecords[seriesIndex].data.push(val);
                    }
                }

                const lineChart: Highcharts.Options = {
                    chart: {
                        marginTop: 50,
                        height: 275,
                        type: 'spline'
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        itemDistance: 80,
                        align: 'center',
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: 0
                    },
                    yAxis: {
                        gridLineWidth: 1,
                        title: {
                            text: 'UTILISATION TREND (%)'
                        }
                    },
                    series: this.lineChartRecords
                };
                this.chart = chart(this.lineChartTarget.nativeElement, lineChart);
            },
            error => {
                console.log(error);
            }
        );
    }

    getItemIndexbyName(seriesName) {
        let position = -1;
        for (let j = 0; j < this.lineChartRecords.length; j++) {
            if (this.lineChartRecords[j].name === seriesName) {
               position = j;
               break;
            }
        }
        return position;
    }

    loadDonutChart() {
        this.dashboardService.getDonutChartAndKPI(this.sharedService.selectedNodeId, this.sharedService.selectedNodeType).subscribe(
            data => {
                this.donutChartData = data.LSTWS;
                this.kpiItems.workTime = this.getDataByName('WORK TIME').toFixed(1);
                this.kpiItems.workTimePercent = this.getPercentDataByName('WORK TIME').toFixed(1);

                this.kpiItems.workTimeLoss = this.getDataByName('WORK TIME LOSS').toFixed(1);
                this.kpiItems.workTimeLossPercent = this.getPercentDataByName('WORK TIME LOSS').toFixed(1);

                this.kpiItems.attendance = this.getDataByName('ATTENDANCE').toFixed(1);
                this.kpiItems.attendancePercent = this.getPercentDataByName('ATTENDANCE').toFixed(1);


                let donutSeries = [];
                donutSeries.push(this.getDonutChartSeriesData('CORE WORK TIME'));
                donutSeries.push(this.getDonutChartSeriesData('NON-CORE WORK TIME'));
                donutSeries.push(this.getDonutChartSeriesData('PRIVATE'));
                donutSeries.push(this.getDonutChartSeriesData('WORK TIME LOSS'));

                const donughtChart: Highcharts.Options = {
                    chart: {
                        renderTo: 'container',
                        type: 'pie'
                    },
                    title: {
                        text: ''
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        pie: {
                            colors: ['#41b3ff', '#4bddf5', '#ff7e90', '#e9455b'],
                            innerSize: '88%',
                            dataLabels: {
                                enabled: false,
                            },
                        }
                    },
                    series: [{
                        data: donutSeries
                    }]
                };

                this.chart = chart(this.donughtChartTarget.nativeElement, donughtChart);
                this.loadDonutCenterChartContain(this.chart);
            },
            error => {
                console.log(error);
            }
        );
    }

    getDonutChartSeriesData(itemName) {
        let series = [];
        series.push(itemName);
        series.push(parseFloat(this.getDataByName(itemName).toFixed(1)));
        return series;
    }

    getDataByName(itemName) {
        var val = 0;
        for (let i = 0; i < this.donutChartData.length; i++) {
            if (this.donutChartData[i].NM === itemName) {
                val = this.donutChartData[i].VAL;
                break;
            }
        }
        return val;
    }

    getPercentDataByName(itemName) {
        var val = 0;
        for (let i = 0; i < this.donutChartData.length; i++) {
            if (this.donutChartData[i].NM === itemName) {
                val = this.donutChartData[i].PVAL;
                break;
            }
        }
        return val;
    }

    loadDonutCenterChartContain(chart) {
        var textX = chart.plotLeft + (chart.plotWidth * 0.5);
        var textY = chart.plotTop + (chart.plotHeight * 0.5);

        var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
        span += '<span style="font-size:65px;color: #1b1e39">60<span style="font-size:20px">%</span></span><br>';
        span += '<span style="font-size:18px; color: #1b1e39;opacity: 0.6;font-family: SourceSansPro;letter-spacing: 1px;">UTILISATION</span>';
        span += '</span>';
    }
}
