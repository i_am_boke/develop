import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/services/apiServices/commonServices/auth.service';
import { SharedService } from '../../../shared/services/apiServices/commonServices/shared.service';
import { LoginCarouselComponent } from '../../../core/login-carousel/login-carousel.component';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment.prod';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
    model: any = {};
    sapienceVersion;
    constructor(private authService: AuthService, private sharedService: SharedService, private router: Router) {
        this.sapienceVersion = environment.version;
    }

    ngOnInit() {
    }

    emailLink() {
        // TODO: Call API to send email to user. After the api call is made, redirect the user to login page and
        // show a message about email sent.
        this.router.navigate(['/login', true]);
    }

    cancelForgotPwd() {
        this.router.navigate(['/login']);
    }

}
