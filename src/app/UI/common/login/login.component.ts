import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../../shared/services/apiServices/commonServices/auth.service';
import { SharedService } from '../../../shared/services/apiServices/commonServices/shared.service';
import { LoginCarouselComponent } from '../../../core/login-carousel/login-carousel.component';
import { environment } from '../../../../environments/environment.prod';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
    model: any = {};
    isInvalidAuth = false;
    isEmailSent = false;
    private sub: any;
    sapienceVersion;

  constructor(private authService: AuthService, private sharedService: SharedService, private router: Router,
              private actRoute: ActivatedRoute) {
      this.isInvalidAuth = false;
      this.sapienceVersion = environment.version;
  }

  ngOnInit() {
      this.sub = this.actRoute.params.subscribe( params => {
          if (params.reset) {
              this.isEmailSent = true;
          }
      });
  }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    forgotPassword() {
        this.router.navigate(['/forgot-password']);
    }

  login() {
      this.authService.login(this.model.username, this.model.password).subscribe(
          data => {
              if (data.RSV >= 0) {
                  data.ROI[0].NT = 0;
                  localStorage.setItem('currentUser', JSON.stringify(data));
                  this.isInvalidAuth = false;
                  this.sharedService.setSelectedDateRangeOnLogin();
                  this.router.navigate(['/main/dashboard']);
              } else {
                  this.isInvalidAuth = true;
              }
          },
          error => {
              this.isInvalidAuth = true;
          }
      );
  }

    loginUser(e) {
      // TODO: Check if both user name and pwd is entered, else give error on UI.
      if (e.keyCode === 13) {
         this.login();
      }
    }
}
