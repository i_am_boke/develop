import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/services/apiServices/commonServices/auth.service';
import { SharedService } from '../../../shared/services/apiServices/commonServices/shared.service';
import { environment } from '../../../../environments/environment.prod';
import { Router } from '@angular/router';

@Component({
    selector: 'app-confirm-password',
    templateUrl: './confirm-password.component.html',
    styleUrls: ['./confirm-password.component.scss']
})
export class ConfirmPasswordComponent implements OnInit {
    model: any = {};
    sapienceVersion;
    isInvalidPwds = false;

    constructor(private authService: AuthService, private sharedService: SharedService, private router: Router) {
        this.sapienceVersion = environment.version;
    }

    ngOnInit() {
    }

    validatePasswordEquality() {
        return this.model.newPassword === this.model.confirmPassword;
    }

    changePwd() {
        // TODO: Call the API to update the pwd. The API is currently not shared.
        // After API call is successful, ask user to login again. So redirect the user to login page on OK button click.
        if (this.validatePasswordEquality()) {
            this.router.navigate(['/login']);
        } else {
            this.isInvalidPwds = true;
        }
    }

    cancelConfirmPwd() {
        this.router.navigate(['/login']);
    }

    updatepwd(e) {
        if (e.keyCode === 13) {
            this.changePwd();
        }
    }
}
